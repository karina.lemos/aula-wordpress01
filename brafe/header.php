<?php ?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php
        if (is_front_page()){
            echo wp_title("");
        }else if(is_page()){ echo wp_title();  echo ' - ';}
        else if(is_search()){ echo 'Busca - ';}
        else if (!(is_404()) && (is_single()) || (is_page())){
            echo wp_title();  echo ' - ';
        }else if(is_404()){ echo 'Não encontrada - ';}
        bloginfo("name")
    ?></title>
    <?php wp_head() ?>
</head>
<body>
    <header>
        <nav>
            <a href="<?php if(is_front_page()){echo "#";} else{echo get_home_url();} ?>"><img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/brafe.png" alt="Logo da Brafé">
            <?php 
            $args = array(
                'menu' => 'principal',
                'theme_location' => 'navegacao',
                'container' => false
            );
            wp_nav_menu($args) ?>
        </nav>
    </header>