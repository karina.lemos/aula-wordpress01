<?php
    // Template Name: Home Page
?>

<?php get_header(); ?>  
    <main>
        <section>
            <div id="confdiv">
                <h1 class="maintitle01"> CAFÉS COM A CARA</h1>
                <h1 class="maintitle02"> DO BRASIL</h1>
                <h5 class="secundarytitle">Direto das fazendas de Minas Gerais</h5>
            </div>
        </section>
        
        <section class="container">
            <h3 class = "misturastyle"> Uma Mistura de </h3>
            <div id = "alinhafotos">
                <div class= "container">
                    <img class="pic1" src="<?php echo get_stylesheet_directory_uri(); ?>/img/cafe-1.jpg" alt="café espresso em formato de coração">
                    <div class="container02 margincont01">
                        <p>amor</p>
                    </div>
                </div>
                <div  class= "container"> 
                    <img class="pic2" src="<?php echo get_stylesheet_directory_uri(); ?>/img/cafe-2.jpg" alt="Café espresso">
                    <div class="container02 margincont02">
                        <p>perfeição</p>
                    </div>
                </div>
            </div>      
            <p class="containertext">O café é uma bebida produzida a partir dos grãos torrados do fruto do cafeeiro. É servido
                    tradicionalmente quente, mas também pode ser consumido gelado. Ele é um estimulante,
                    por possuir cafeína — geralmente 80 a 140 mg para cada 207 ml dependendo do método de
                    preparação.
            </p>
        </section>

        <section class="confisec03">
            <div class="alinhabolinhas"> 
                <div class="colordiv01">
                    <div class="colordiv02 pulse"></div>
                </div>
                <div class="colordiv03">
                    <div class="colordiv04"></div>
                </div>
                <div class="colordiv05" >
                    <div class="colordiv06"></div>
                </div>
            </div>
            <div class="alinhacontext">
                <div class = "alinhacontext2">
                    <h2 class="tit01">Paulista</h2>
                    <p class="context01">As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850,
                                tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará.
                    </p>
                </div>
                <div class = "alinhacontext2">
                    <h2  class="tit02">Carioca</h2>
                    <p class="context02">As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850,
                                tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará.
                    </p>
                </div>
                <div class = "alinhacontext2">
                    <h2  class="tit03">Mineiro</h2>
                    <p class="context03">As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850,
                                tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará.
                    </p>
                </div>
            </div>
            <button class="button buttonefeito">SAIBA MAIS</button>
        </section>

        <section class="divisao">
            <div class="alinharimgelist"> 
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/botafogo.jpg" alt="espaço botafogo">
                <ul>
                    <h2 class="titlee02"> Botafogo</h2>
                    <p class="containertext03">As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850,
                        tentou-se o cultivo.
                    </p>
                    <button class = "buttontwo buttontwoefeito" > VER MAPA</button>
                </ul>
            </div>
            <div class="alinharimgelist">
                <img clas = "picsty02" src="<?php echo get_stylesheet_directory_uri(); ?>/img/iguatemi.jpg" alt="espaço Iguatemi">
                <ul>
                    <h2 class="titlee02">Iguatemi</h2>
                    <p class="containertext03">As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850,
                        tentou-se o cultivo.
                    </p>
                    <button class = "buttontwo buttontwoefeito"> VER MAPA</button>
                </ul>
            </div>
            <div class="alinharimgelist">
                <img clas = "picsty02" src="<?php echo get_stylesheet_directory_uri(); ?>/img/mineirao.jpg" alt="espaço Mineirão">
                <ul>
                    <h2 class="titlee02"> Mineirão</h2>
                    <p class="containertext03">As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850,
                        tentou-se o cultivo.
                    </p>
                    <button class = "buttontwo buttontwoefeito"> VER MAPA</button>
                </ul>
            </div>
        </section>

        <section class="configsec05">
            <div class = "container04">
                <h2 class= "fonthmmt">Assine Nossa Newsletter</h2>
                <p class= "fonthmmt02">promoções e eventos mensais</p>
            </div> 
            <form class = "configform" action="">
                <input class= "text-input" type="e-mail" placeholder="Digite seu e-mail">
                <button class="buttonthree">ENVIAR</button>
            </form>
        </section>
    </main>
<?php get_footer(); ?> 